// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or vendor/assets/javascripts of plugins, if any, can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file.
//
// Read Sprockets README (https://github.com/sstephenson/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require jquery
//= require jquery_ujs
//= require twitter/bootstrap
//= require_tree ./common
//= require_tree .

jQuery(document).ready(function ($) {
  jQuery('#slide-content').css('width', jQuery(window).width())
  var options = { $AutoPlay: true };
  var jssor_slider1 = new $JssorSlider$('slider1_container', options);

  jQuery('a[href*=#]').click(function() {
    if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'')
    && location.hostname == this.hostname) {
      var jQuerytarget = jQuery(this.hash);
      jQuerytarget = jQuerytarget.length && jQuerytarget
      || jQuery('[name=' + this.hash.slice(1) +']');
      if (jQuerytarget.length) {
        var targetOffset = jQuerytarget.offset().top;
        jQuery('html,body')
        .animate({scrollTop: targetOffset}, 1000);
       return false;
      }
    }
  });

  jQuery(window).scroll(function() {
    if (jQuery(this).scrollTop() > 1) {
      jQuery('#ABdev_main_header').addClass('transparent')
    } else {
      jQuery('#ABdev_main_header').removeClass('transparent')
    }
});
});
